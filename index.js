const express = require("express");

const port = 4000;

(async ()=> {
  
  const server = express();
  
  server.get("/", (req, res)=> {
    res.end("hola");
  })
  
  server.listen(port, ()=> {
    console.log(`Server running on ${port}`);
  });
})();
